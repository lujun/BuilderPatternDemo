package co.lujun.builderpatterndemo;

/**
 * Created by lujun on 2015/9/14.
 */
public class ProductBuilder implements Builder {

    Product product;

    public ProductBuilder(){
        product = new Product();
    }

    @Override
    public void buildAge() {
        product.setAge(5);
    }

    @Override
    public void buildName() {
        product.setName("Benz");
    }

    @Override
    public void buildWidth() {
        product.setWidth(2.55f);
    }

    @Override
    public void buileHeight() {
        product.setHeight(1.50f);
    }

    @Override
    public Product buildProduct() {
        return product;
    }
}
