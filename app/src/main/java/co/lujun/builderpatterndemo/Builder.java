package co.lujun.builderpatterndemo;

/**
 * Created by lujun on 2015/9/14.
 */
public interface Builder {

    void buildName();
    void buildAge();
    void buildWidth();
    void buileHeight();

    Product buildProduct();
}
