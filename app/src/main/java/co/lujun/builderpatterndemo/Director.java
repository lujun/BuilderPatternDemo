package co.lujun.builderpatterndemo;

/**
 * Created by lujun on 2015/9/14.
 */
public class Director {

    private Builder builder;

    public Director(Builder builder){
        this.builder = builder;
    }

    public Product makeProduct(){
        builder.buildAge();
        builder.buildName();
        builder.buildWidth();
        builder.buileHeight();
        return builder.buildProduct();
    }
}
